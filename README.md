#Annales

##Introduction

Dépôt compilant tous les scans personnels qui ont permis d'approvisionner Mastercorp et Epiportal. Et anciennement Epinnal. Il s'agit ici d'un dépôt personnel qui n'a pas pour but de concurrencer Mastercorp, Epiportal ou encore Epinnal.

Je suis actuellement entrain d'essayer d'ajouter les corrigés aux TD dans la plupart des matières. Certaines de mes prises de notes ne sont pas aussi complètes, c'est pourquoi je ne garantie rien et j'espère que cela pourra vous aider à propos des partiels et des rattrapages.

##Refonte partielle du dépôt

En raison d'un différend avec le dépôt officiel des Annales d'EPITA, une partie des documents vont disparaître. En revanche, vous trouverez ici, ce qu'Epinnal ne veut pas mettre en ligne (comme les sujets et leurs corrections). J'envisage également d'arrêter la numérisation des annales (qui se trouveraient sur Epinnal) en prévision des 4 années qu'il reste.

Je prévois par la présente refonte de rendre supplémentaires les deux dépôts. Qui respecte l'exacte définition de deux espaces vectoriels supplémentaires. C'est-à-dire : Epinnal ⋂ HyperAnnal = {0} et Epinnal + HyperAnnal = 42
